# Free Circle Helper Bot
This software consists in a bot created to help you administrate telegram groups and supergroups.

# What does it do?
With this bot you can set a welcome message, rules message, anti spam filters, warning messages and send 
simple public polls!

# How to install?
Before you start the bot you need to create a sqlite3 database file, and import the DB init file located
 in `/Classes/DatabaseClasses/dbinit.sql`.
 (if you change the database position, you need to change the path in the config.ini file)
 
After that, you can use `pip install -r requirements.txt` to install the python required libraries.

# Starting the bot
If the installation went smooth, you can start and use this bot, and you can launch the following command: `python3 Main.py <your token>`.

Then, you can add your bot in your groups as administrator and enjoy it! (**Remember that the bot needs to be administrator to work properly**)

# How to use
Now the bot is running in your groups, and you can use the command `/settings` as a direct message to the bot and manage 
every group with our amazing user friendly settings menu.