import unittest
from sqlalchemy.pool import SingletonThreadPool
from Main import *
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
path = config['DATABASE']['PATH']

try:
    TOKEN = sys.argv[1]
except IndexError:
    message = 'Please Write the telegram token as argument when you start the bot, for example: ' \
              '"python3 Main.py xadxasa42XAG5XARFXAF%SXRFADFF_G32"'
    # raise IndexError(message)
    TOKEN = '488001898:AAGKjYJ4XxpedIa9Z-762vSc31QmxxfM_r0'  # to run tests, comment the line above, and decomment this one

updater = Updater(token=TOKEN, use_context=True)
bot = updater.bot
dispatcher = updater.dispatcher


class FakeBot:
    def __init__(self):
        self.user = Struct(**{'id': 1001})
        self.admin = Struct(**{'user': self.user})
        self.chat_amministrators = [self.admin]

    def send_message(self, chat_id=None, text=None, reply_markup=None, parse_mode='STD', message_id=None):
        print(f'messaggio inviato a ({chat_id}) contenente: {text} \n '
              f'Reply_markup : {reply_markup}\n'
              f'parse mode : {parse_mode}\n'
              f'message_id : {message_id}\n')
        return Struct(**{'chat_id': 1002, 'message_id': 23193})

    def edit_message_text(self, chat_id=None, text=None, reply_markup=None, parse_mode='STD', message_id=None):
        print(f'messaggio inviato a ({chat_id}) contenente: {text} \n '
              f'Reply_markup : {reply_markup}\n'
              f'parse mode : {parse_mode}\n'
              f'message_id : {message_id}\n')

    def get_chat_administrators(self, chat_id):
        return self.chat_amministrators

    def kick_chat_member(self, chat_id=None, user_id=None, until_date=0):
        print(f'chat_id : {chat_id}\n'
              f'user_id : {user_id}\n'
              f'untile date : {until_date}')
 
    def restrict_chat_member(self, permissions=None, chat_id=None, user_id=None, until_date=0):
        print(f'chat_id : {chat_id}\n'
              f'user_id : {user_id}\n'
              f'untile date : {until_date}'
              f'permissions : {permissions}')


class FakeContext:
    def __init__(self, bot):
        self.bot = bot


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


class MyTestCase(unittest.TestCase):
    engine = create_engine(path, echo=False, poolclass=SingletonThreadPool)
    Session = sessionmaker(expire_on_commit=True)
    Session.configure(bind=engine)
    session = Session()
    bot = FakeBot()
    context = FakeContext(bot)
    p1 = Struct(**{'id': 1001, 'first_name': 'test', 'last_name': 'test', 'username': 'test'})
    p2 = Struct(**{'id': 1011, 'first_name': 'test', 'last_name': 'test', 'username': 'test'})
    effective_chat_dict = {
        'id': 1002,
        'title': 'test',
        'type': 'group,'
    }
    effective_chat = Struct(**effective_chat_dict)
    effective_user = Struct(**{'id': 1001, 'first_name': 'test', 'last_name': 'test', 'username': 'test'})
    from_user_dict = {'id': 1001, 'first_name': 'test', 'last_name': 'test', 'username': 'test'}
    from_user = Struct(**from_user_dict)
    chat_dict = {
        'id': 1002,
        'type': 'private',
    }
    chat = Struct(**chat_dict)
    message_dict = {
        'from_user': from_user,
        'chat': chat,
        'chat_id': 1002,
        'message_id': 1000213,
        'new_chat_members': [p1, p2],
        'text': '1',
        'reply_to_message': Struct(**{
            'from_user': from_user,
            'chat': chat,
            'chat_id': 1002,
            'message_id': 1000213, }),
    }
    message = Struct(**message_dict)
    callback_query = Struct(**{'data': 'vote:4', 'message': message})
    update_dict = {
        'effective_chat': effective_chat,
        'effective_user': effective_user,
        'message': message,
        'callback_query': callback_query,
    }
    update = Struct(**update_dict)

    def test_start(self):
        result = start(self.update, self.context)
        self.assertIsNone(result)

    def test_show_menu(self):
        result = show_menu(self.update, self.context)

    def test_create_menu(self):
        group = Struct(**{'title': 'test'})
        result = create_menu(1002, group, context=self.context)

    def test_show_rules(self):
        result = show_rules(self.update, self.context)

    def test_show_link(self):
        result = show_link(self.update, self.context)

    def test_welcome(self):
        result = welcome(self.update, self.context)

    def test_check_if_blacklisted(self):
        result = check_if_blacklisted(1001)
        self.assertFalse(result)

    def test_send_poll(self):
        result = send_poll(self.update, self.context)

    def test_button(self):
        button(self.update, self.context)
        self.update.callback_query.data = 'session-group:1'
        button(self.update, self.context)
        self.update.callback_query.data = 'selectgroup:home'
        button(self.update, self.context)
        self.update.callback_query.data = 'menu:'
        button(self.update, self.context)
        self.update.callback_query.data = 'warnings:home'
        button(self.update, self.context)
        self.update.callback_query.data = 'warnings:num_trigger'
        button(self.update, self.context)
        self.update.callback_query.data = 'warnings:spoiler_time'
        button(self.update, self.context)
        self.update.callback_query.data = 'warnings:punishment'
        button(self.update, self.context)
        self.update.callback_query.data = 'warnings:punishment'
        button(self.update, self.context)
        self.update.callback_query.data = 'mute:time'
        button(self.update, self.context)
        self.update.callback_query.data = 'welcomemessage:start'
        button(self.update, self.context)
        self.update.callback_query.data = 'welcomemessage:save'
        button(self.update, self.context)
        self.update.callback_query.data = 'welcomemessage:undo'
        button(self.update, self.context)
        self.update.callback_query.data = 'rules:start'
        button(self.update, self.context)
        self.update.callback_query.data = 'rules:save'
        button(self.update, self.context)
        self.update.callback_query.data = 'rules:undo'
        button(self.update, self.context)
        self.update.callback_query.data = 'link:start'
        button(self.update, self.context)
        self.update.callback_query.data = 'link:save'
        button(self.update, self.context)
        self.update.callback_query.data = 'link:undo'
        button(self.update, self.context)
        self.update.callback_query.data = 'createpoll:continue1'
        button(self.update, self.context)
        self.update.callback_query.data = 'createpoll:home'
        button(self.update, self.context)
        self.update.callback_query.data = 'createpoll:continue2'
        button(self.update, self.context)
        self.update.callback_query.data = 'createpoll:finish'
        button(self.update, self.context)
        self.update.callback_query.data = 'createpoll:send'
        button(self.update, self.context)
        self.update.callback_query.data = 'createpoll:undo'
        button(self.update, self.context)
        self.update.callback_query.data = 'createpoll:modify1'
        button(self.update, self.context)
        self.update.callback_query.data = 'grouptosendpoll:1'
        button(self.update, self.context)

    def test_update_poll_votes(self):
        answer = Question.get_answer(4, self.session)
        question = answer.question
        result = update_poll_votes(question, self.message, self.context)

    def test_set_welcome_message(self):
        result = set_welcome_message(self.update, self.context)

    def test_set_rules(self):
        result = set_rules(self.update, self.context)

    def test_set_warnings(self):
        result = set_warnings(self.update, self.context)

    def test_check_session(self):
        result = check_session(self.update, self.context, 1001)

    def test_select_session(self):
        result = select_session(self.update, self.context, 1001, 1, self.message)

    def test_is_admin(self):
        result = is_admin(1001, 1002, self.context)

    def test_refresh_admins(self):
        result = refresh_admins(1002, self.context)

    def test_init_user_group(self):
        result = init_user_group(self.update, self.context)

    def test_warn(self):
        result = warn(self.update, self.context)

    def test_punish(self):
        group = Group.get_by_id(1, self.session)
        user = User.check_user_exist(1001, self.session)
        result = punish(user, group, 'warnings', self.context)

    def test_echo(self):
        user = User.check_user_exist(1001, session)
        user.session[0].other = 'x'
        result = echo(self.update, self.context)
        user.session[0].other = 'xwm#1:'
        result = echo(self.update, self.context)
        user.session[0].other = 'xr#1:'
        result = echo(self.update, self.context)
        user.session[0].other = 'xl#1:'
        result = echo(self.update, self.context)
        user.session[0].other = 'xq#1:'
        result = echo(self.update, self.context)
        user.session[0].other = 'xq#3:'
        result = echo(self.update, self.context)
        user.session[0].other = 'xq#5:'
        result = echo(self.update, self.context)


if __name__ == '__main__':
    unittest.main()
