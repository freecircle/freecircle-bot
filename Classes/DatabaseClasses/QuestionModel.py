import datetime
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.exc import IntegrityError, InterfaceError, InvalidRequestError
from sqlalchemy.orm import sessionmaker
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
path = config['DATABASE']['PATH']
Base = declarative_base()
Engine = create_engine(path, echo=True)


""" 
    fare doppia ereditarietà (Base,Session), e quindi poi levare tutti i  metodi statici
    ad esempio prendiamo check_answer di Question fai QUestion.created_at.desc()
    perchè visto che hai usato un orm fai metodi no funzioni.
    self.created_at.desc() e al posto di Question come par di query usa in tutti self.__class__
    poi magari potresti anche separarli una classe per file
"""
class Util:

    @staticmethod
    def get_new_session(engine=None):
        if not engine:
            engine = create_engine(path, echo=True)
        Session = sessionmaker()
        Session.configure(bind=engine)
        return Session()

class Question(Base):
    __tablename__ = 'question'

    id = Column(Integer, primary_key=True)
    type = Column(Integer)  # 0 => public; 1 => anonymous.
    content = Column(String)
    status = Column(Integer)  # 0 => poll not created yet; 1 => created, but not closed; 1 => closed.
    creator_id = Column(Integer)
    created_at = Column(Integer)

    answers = relationship("Answer", back_populates="question")

    def __repr__(self):
        return f"<Question(ID={self.id}, telegram_id={self.creator_id}, content={self.content}, status={self.status}," \
               f" created_at={self.created_at})>"

    @staticmethod
    def check_answer(user_id, content, session):
        question = session.query(Question).filter_by(creator_id=user_id).filter(Question.status == 0) \
            .order_by(Question.created_at.desc()).first()
        for answer in question.answers:
            if answer.content == content:
                return True
        return False

    @staticmethod
    def init_question(question, user_id, session):
        try:
            question_query = Question(type=0, content=question, status=0, creator_id=user_id,
                                      created_at=datetime.datetime.now().timestamp())
            session.add(question_query)
            session.commit()
            return question_query
        except InvalidRequestError:
            session.rollback()
            new_session = Util.get_new_session(engine=session.bind)
            return Question.init_question(question, user_id, new_session)

    @staticmethod
    def add_answer(user_id, content, session):
        question = session.query(Question).filter_by(creator_id=user_id).filter(Question.status == 0) \
            .order_by(Question.created_at.desc()).first()
        if question:
            question.answers += [Answer(question_id=question.id, content=content)]
            session.add(question)
            session.commit()
            return True
        else:
            return False

    @staticmethod
    def complete_poll(user_id, session):
        question = session.query(Question).filter_by(creator_id=user_id).filter(Question.status == 0) \
            .order_by(Question.created_at.desc()).first()
        if question:
            question.status = 1
            session.add(question)
            session.commit()
            return True
        else:
            return False

    @staticmethod
    def finish_poll(user_id, session):
        question = session.query(Question).filter_by(creator_id=user_id).filter(Question.status == 1) \
            .order_by(Question.created_at.desc()).first()
        if question:
            question.status = 2
            session.add(question)
            session.commit()
            return question
        else:
            return False

    @staticmethod
    def get_last_question(user_id, session):
        return session.query(Question).filter_by(creator_id=user_id).filter(Question.status == 1) \
            .order_by(Question.created_at.desc()).first()

    @staticmethod
    def get_answer(answer_id, session):
        return session.query(Answer).filter_by(id=answer_id).first()

    def check_if_answered(self, user_id):
        for answer in self.answers:
            for vote in answer.users:
                if vote.user_id == user_id:
                    return vote
        return False


class Answer(Base):
    __tablename__ = 'answer'

    id = Column(Integer, primary_key=True)
    question_id = Column(Integer, ForeignKey('question.id'))
    content = Column(String)

    def __repr__(self):
        return f"<Answer(question_id={self.question_id}, content={self.content})>"

    question = relationship("Question", back_populates="answers")
    users = relationship("UserAnswer", back_populates="answer")


class UserAnswer(Base):
    __tablename__ = 'user_answer'

    answer_id = Column(Integer, ForeignKey('answer.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)

    def __repr__(self):
        return f"<Answer(answer_id={self.answer_id}, user_id={self.user_id})>"

    user = relationship("User", back_populates="answers")
    answer = relationship("Answer", back_populates="users")


class GroupUser(Base):
    __tablename__ = 'group_user'

    group_id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    is_admin = Column(Integer)

    def __repr__(self):
        return f"<Group(group={self.group_id}, user={self.user_id}, admin={self.is_admin})>"

    user = relationship("User", back_populates="groups")
    group = relationship("Group", back_populates="users")

    @staticmethod
    def connect(user_id, group_id, is_admin, session):
        connection = session.query(GroupUser).filter_by(user_id=user_id).filter_by(group_id=group_id).first()
        if connection:
            if connection.is_admin != is_admin:
                connection.is_admin = is_admin
                session.add(connection)
                session.commit()
        else:
            connection = GroupUser(user_id=user_id, group_id=group_id, is_admin=is_admin)
            session.add(connection)
            session.commit()
        return True


class Group(Base):
    __tablename__ = 'group'

    id = Column(Integer, primary_key=True)
    telegram_id = Column(Integer)
    type = Column(String)
    link = Column(String)
    rules = Column(String)
    title = Column(String)
    welcome_message = Column(String)

    def __repr__(self):
        return f"<Group(telegram_id={self.telegram_id}, type={self.type}, link={self.link}, title={self.title})>"

    users = relationship("GroupUser", back_populates="group")
    session = relationship("UserSession", back_populates="group")
    setting = relationship("GroupSettings", back_populates="group")
    warnings = relationship("Warnings", back_populates="group")

    def set_welcome_message(self, message, session):
        if len(message) < 5000:
            self.welcome_message = message
            session.commit()
            return True
        return False

    def set_rules(self, message, session):
        if len(message) < 5000:
            self.rules = message
            session.commit()
            return True
        return False

    def set_link(self, message, session):
        if len(message) < 5000:
            self.link = message
            session.commit()
            return True
        return False

    def get_user_warnings(self, user_id):
        try:
            warnings = []
            time = self.setting[0].warning_spoiler_time
            for warn in self.warnings:
                if warn.user_id == user_id and datetime.datetime.strptime(warn.created_at, '%Y-%m-%d %H:%M:%S.%f') > \
                        datetime.datetime.now() - datetime.timedelta(hours=time):
                    warnings.append(warn)
            return warnings
        except (IndexError, InterfaceError, TypeError, InvalidRequestError):
            return []

    def refresh_admins(self, admin_list, session):
        try:
            admin_list_id = []
            for admin in admin_list:
                admin_list_id.append(admin.user.id)
            for user in self.users:
                if user.user.telegram_id in admin_list_id:
                    user.is_admin = 1
                else:
                    user.is_admin = 0
                    if user.user.session[0].group_id == self.id:
                        session.delete(user.user.session[0])
            session.add(self)
            session.commit()
            return True
        except InvalidRequestError:
            return False

    @staticmethod
    def get_by_id(group_id, session):
        return session.query(Group).filter_by(id=group_id).first()

    @staticmethod
    def group_exist(telegram_id, session):
        try:
            return session.query(Group).filter_by(telegram_id=telegram_id).first()
        except InvalidRequestError:
            session.rollback()
            new_session = Util.get_new_session(engine=session.bind)
            return Group.group_exist(telegram_id, new_session)

    @staticmethod
    def init_group(telegram_id, type, title, session):
        try:
            group = Group.group_exist(telegram_id, session)
            if group:
                if group.type == type and group.title == title:
                    pass
                else:
                    group.type = type
                    group.title = title
                    session.commit()
            else:
                group = Group(telegram_id=telegram_id, type=type, title=title)
                session.add(group)
                session.commit()
            return group
        except InvalidRequestError:
            session.rollback()
            new_session = Util.get_new_session(engine=session.bind)
            return Group.init_group(telegram_id, type, title, new_session)


class UserSession(Base):
    __tablename__ = 'session'

    id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    group_id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    other = Column(String)

    user = relationship("User", back_populates="session")
    group = relationship("Group", back_populates="session")

    def get_ready_to_write(self, tag='xreeady:'):
        self.other = tag
        return True

    def stop_ready_to_write(self, session):
        self.other = 'x'
        session.commit()
        return True

    @staticmethod
    def select_group(user, group_id, session):
        try:
            user_session = user.session[0]
            if user_session:
                user_session.group_id = group_id
            else:
                user_session = UserSession(id=user.id, group_id=group_id, other=" ")
            session.add(user_session)
            session.commit()
            return user_session
        except IndexError:
            user_session = UserSession(id=user.id, group_id=group_id, other=" ")
            session.add(user_session)
            session.commit()
            return user_session
        except InvalidRequestError:
            session.rollback()
            new_session = Util.get_new_session(engine=session.bind)
            return UserSession.select_group(user, group_id, new_session)


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    telegram_id = Column(Integer)
    first_name = Column(String)
    last_name = Column(String)
    username = Column(String)
    warn = Column(Integer, default=0)

    def __repr__(self):
        return f"<User(telegram_id={self.telegram_id}, first_name={self.first_name}, username={self.username}," \
               f" warn={self.warn})>"

    groups = relationship("GroupUser", back_populates="user")
    session = relationship("UserSession", back_populates="user")
    answers = relationship("UserAnswer", back_populates="user")
    warnings = relationship("Warnings", back_populates="user")

    def get_groups_where_administrator(self):
        groups_list = []
        for connection in self.groups:
            if connection.is_admin:
                groups_list.append(connection.group)
        return groups_list

    def get_user_groups(self):
        try:
            groups_list = []
            for connection in self.groups:
                groups_list.append(connection.group)
            return groups_list
        except (InterfaceError, TypeError):
            return []

    @staticmethod
    def check_user_exist(telegram_id, session):
        return session.query(User).filter_by(telegram_id=telegram_id).first()

    @staticmethod
    def init_user(telegram_id, first_name, last_name, username, session):
        try:
            user = User.check_user_exist(telegram_id, session)
            if user:
                if user.first_name == first_name and user.last_name == last_name and user.username == username:
                    pass
                else:
                    user.first_name = first_name
                    user.last_name = last_name
                    user.username = username
                    session.commit()
            else:
                user = User(telegram_id=telegram_id, first_name=first_name, last_name=last_name, username=username)
                session.add(user)
                session.commit()
            return user
        except InvalidRequestError:
            session.rollback()
            new_session = Util.get_new_session(engine=session.bind)
            return User.init_user(telegram_id, first_name, last_name, username, new_session)

    @staticmethod
    def vote(user_id, answer_id, session):
        try:
            vote = UserAnswer(user_id=user_id, answer_id=answer_id)
            session.add(vote)
            session.commit()
        except IntegrityError:
            pass
        return True


class GroupSettings(Base):
    __tablename__ = 'group_setting'

    id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    warnings = Column(Integer)  # number of warning to trigger the punishment
    warning_spoiler_time = Column(Integer)
    warning_punishment = Column(Integer)  # 0=nothing 1=mute 2=kick 3=ban
    banned_word_punishment = Column(Integer)
    mute_time = Column(Integer)
    message_log = Column(Integer)
    meteo_token = Column(String)
    news_token = Column(String)
    language = Column(String)

    def __repr__(self):
        return f"<GroupSettings(group_id={self.id}, Warnings=[max:{self.warnings}, duration:{self.warning_spoiler_time}, " \
               f"punishment:{self.warning_punishment}], banned_worlds_punishment={self.banned_word_punishment}, " \
               f"mute_time={self.mute_time}, group_language={self.language})>"

    group = relationship("Group", back_populates="setting")

    def set_warnings(self, number):
        self.warnings = number
        return True

    def set_warnings_spoiler_time(self, number):
        self.warning_spoiler_time = number
        return True

    def set_warnings_punishment(self, number):
        self.warning_punishment = number
        return True

    def set_mute_time(self, number):
        self.mute_time = number
        return True

    @staticmethod
    def init_settings(group, session):
        try:
            if not group.setting:
                settings = GroupSettings(id=group.id, warnings=3, warning_spoiler_time=12, warning_punishment=0,
                                         banned_word_punishment=0, mute_time=10, message_log=0)
                session.add(settings)
                session.commit()
            else:
                settings = group.setting[0]
            return settings
        except InvalidRequestError:
            session.rollback()
            new_session = Util.get_new_session(engine=session.bind)
            return GroupSettings.init_settings(group, new_session)


class Warnings(Base):
    __tablename__ = 'warning'

    group_id = Column(Integer, ForeignKey('group.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    created_at = Column(Integer, primary_key=True)

    def __repr__(self):
        return f"<Warning(group={self.group_id}, user_id={self.user_id}, time={self.created_at})>"

    user = relationship("User", back_populates="warnings")
    group = relationship("Group", back_populates="warnings")

    @staticmethod
    def warn_user(user_id, group_id, session):
        try:
            warn = Warnings(user_id=user_id, group_id=group_id, created_at=datetime.datetime.now())
            session.add(warn)
            session.commit()
            return True
        except InvalidRequestError:
            session.rollback()
            new_session = Util.get_new_session(session.bind)
            return Warnings.warn_user(user_id, group_id, new_session)


if __name__ == '__main__':  # It's for debug

    engine = create_engine(path, echo=True)
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
