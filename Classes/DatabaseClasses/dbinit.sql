BEGIN TRANSACTION;
CREATE TABLE 'user_answer'
    (
    answer_id integer NOT NULL,
    user_id integer NOT NULL,
    PRIMARY KEY(answer_id, user_id),
    FOREIGN KEY(answer_id) REFERENCES answer(id),
    FOREIGN KEY(user_id) REFERENCES user(id)
    );
CREATE TABLE IF NOT EXISTS "answer" (
	"id"	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"question_id"	integer NOT NULL,
	"content"	varchar(100) NOT NULL,
	FOREIGN KEY("question_id") REFERENCES "question"("id")
);
CREATE TABLE IF NOT EXISTS "question" (
	"id"	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"type"	integer NOT NULL,
	"content"	varchar(100) NOT NULL,
	"status"	integer NOT NULL DEFAULT 0,
	"creator_id"	integer NOT NULL,
	"created_at"	timestamp NOT NULL
);
CREATE TABLE IF NOT EXISTS "user" (
	"id"	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"telegram_id"	integer NOT NULL UNIQUE,
	"first_name"	varchar(100) NOT NULL,
	"last_name"	varchar(100),
	"username"	varchar(100),
	"warn"	integer NOT NULL DEFAULT 0
);
CREATE TABLE IF NOT EXISTS "group_user" (
	"group_id"	integer NOT NULL,
	"user_id"	integer NOT NULL,
	"is_admin"  integer DEFAULT NULL,
	PRIMARY KEY("group_id", 'user_id'),
	FOREIGN KEY("group_id") REFERENCES "group"("id"),
	FOREIGN KEY("user_id") REFERENCES "user"("id")
);
CREATE TABLE IF NOT EXISTS "group" (
	"id"	integer NOT NULL PRIMARY KEY AUTOINCREMENT,
	"telegram_id"	integer NOT NULL UNIQUE,
	"type"	varchar(20) DEFAULT NULL,
	"link"	TEXT,
	"rules"	TEXT,
	"title"	varchar(250),
	"welcome_message"	TEXT
);
CREATE TABLE IF NOT EXISTS "warning" (
	"group_id"	integer NOT NULL,
	"user_id"	integer NOT NULL,
	"created_at"	timestamp NOT NULL,
	PRIMARY KEY("group_id", "user_id", "created_at"),
	FOREIGN KEY("group_id") REFERENCES "group"("id"),
	FOREIGN KEY("user_id") REFERENCES "user"("id")
);
CREATE TABLE IF NOT EXISTS "group_setting" (
	"id"	integer NOT NULL,
	"warnings"	integer NOT NULL,
	"warning_spoiler_time"	integer NOT NULL,
	"warning_punishment"	integer NOT NULL,
	"banned_word_punishment"	integer NOT NULL,
	"mute_time"	integer NOT NULL,
	"message_log"	integer NOT NULL,
	"meteo_token"	varchar(100) DEFAULT NULL,
	"news_token"	varchar(100) DEFAULT NULL,
	"language"	varchar(2) DEFAULT "it",
	PRIMARY KEY("id"),
	FOREIGN KEY("id") REFERENCES "group"("id")
);
CREATE TABLE IF NOT EXISTS "banned_word" (
	"id"	integer NOT NULL,
	"group_id"	integer NOT NULL,
	"content"	varchar(100) NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY("group_id") REFERENCES "group"("id")
);
CREATE TABLE IF NOT EXISTS "automatic_answer" (
	"id"	integer NOT NULL,
	"group_id"	integer NOT NULL,
	"trigger"	varchar(100) NOT NULL,
	"echo"	varchar(100) NOT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY("group_id") REFERENCES "group"("id")
);
CREATE TABLE IF NOT EXISTS "session" (
	"id"	integer NOT NULL UNIQUE,
	"group_id"	integer NOT NULL,
	"other"	varchar(100) DEFAULT NULL,
	PRIMARY KEY("id"),
	FOREIGN KEY("id") REFERENCES "user"("id"),
	FOREIGN KEY("group_id") REFERENCES "group"("id")
);
COMMIT;
