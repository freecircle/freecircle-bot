#!/usr/bin/env bash

set -e

if [ ! -d "/home/$USER/freecircle-bot" ]; then
  echo "Please clone fcbot repository under /home/$USER/freecircle-bot, before launching the install script!"
fi

virtualenv_command=`command -v virtualenv`

if [ "${virtualenv_command}" == "" ]; then
  echo "You must install python3-virtualenv first."
  exit 1
else
  echo "Good! virtualenv is installed under ${virtualenv_command}"
fi

sqlite3_command=`command -v sqlite3`

if [ "${sqlite3_command}" == "" ]; then
  echo "You must install sqlite3 first."
  exit 1
else
  echo "Good! sqlite3 is installed under ${sqlite3_command}"
fi

if [ ! -d "/home/$USER/fcbot-var" ]; then
  mkdir -p /home/$USER/fcbot-var
fi

if [ ! -f "/home/$USER/fcbot-var/token.txt" ]; then
  echo "Token is missing, aborting installation!"
  echo "Please note that in CI, this is normal for the first installation.
If this is the case, simply put the token in /home/$USER/fcbot-var/token.txt, and restart the pipeline."
  exit 1
fi

TOKEN=`cat /home/$USER/fcbot-var/token.txt`

if [ -d "/home/$USER/freecircle-bot/venv" ]; then
  rm -r /home/$USER/freecircle-bot/venv
fi

${virtualenv_command} -p python3 venv

venv/bin/pip3 install -r requirements.txt

if [ ! -f "/home/$USER/fcbot-var/database.db" ]; then
  touch /home/$USER/fcbot-var/database.db
  ${sqlite3_command} /home/$USER/fcbot-var/database.db < /home/$USER/freecircle-bot/Classes/DatabaseClasses/dbinit.sql
fi

if [ ! -d "/home/$USER/.config/systemd/user" ]; then
  mkdir -p /home/$USER/.config/systemd/user
fi

firejail_command=`command -v firejail`

if [ "${firejail_command}" == "" ]; then
  cp fcbot.service /home/$USER/.config/systemd/user/
  sed -i "s#USER#$USER#g" /home/$USER/.config/systemd/user/fcbot.service
  sed -i "s#TOKEN#$TOKEN#g" /home/$USER/.config/systemd/user/fcbot.service
  systemctl --user daemon-reload
  systemctl --user enable fcbot.service
  systemctl --user restart fcbot.service
  echo "Done!"
else
  echo "Installing with firejail support..."
  cp fcbot-firejail.service /home/$USER/.config/systemd/user/
  sed -i "s#USER#$USER#g" /home/$USER/.config/systemd/user/fcbot-firejail.service
  sed -i "s#TOKEN#$TOKEN#g" /home/$USER/.config/systemd/user/fcbot-firejail.service
  systemctl --user daemon-reload
  systemctl --user enable fcbot-firejail.service
  systemctl --user restart fcbot-firejail.service
  echo "Done!"
fi

exit 0