import unittest
import random
from Classes import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import SingletonThreadPool
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
path = config['DATABASE']['PATH']

engine = create_engine(path, echo=False, poolclass=SingletonThreadPool)
Session = sessionmaker(expire_on_commit=True, autoflush=True)
Session.configure(bind=engine)
session = Session()


class MyTestCase(unittest.TestCase):
    question = Question.init_question('test_question', 1001, session)
    user = User.init_user(1001, 'test_first_name', 'test_last_name', 'test_username', session)
    group = Group.init_group(1002, 'public', 'test_group', session)
    user_session = UserSession.select_group(user, 1, session)
    group_settings = GroupSettings.init_settings(group, session)

    # Question Methods
    def test_init_question(self):
        result = Question.init_question('test_super_question', 1001, session)
        if result:
            self.assertIsInstance(result, Question)

    def test_add_answer(self):
        result = Question.add_answer(1001, 'answer_test', session)
        self.assertEqual(result, True)

    def test_add_answer_2(self):
        result = Question.add_answer(100231, 'answer_test', session)
        self.assertEqual(result, False)

    def test_complete_poll(self):
        Question.init_question('test_question', 1001, session)
        Question.add_answer(1001, 'answer_test', session)
        result = Question.complete_poll(1001, session)
        self.assertEqual(result, True)

    def test_get_last_question(self):
        Question.init_question('test_question', 1001, session)
        Question.add_answer(1001, 'answer_test', session)
        Question.complete_poll(1001, session)
        result = Question.get_last_question(1001, session)
        self.assertIsInstance(result, Question)

    def test_check_answer(self):
        string = str(random.randint(0, 199999)) + 'test'
        result = Question.check_answer(1001, string, session)
        self.assertEqual(result, False)

    def test_check_if_answered(self):
        Question.add_answer(1001, 'answer_test', session)
        Question.complete_poll(1001, session)
        question = Question.get_last_question(1001, session)
        User.vote(1, question.answers[0].id, session)
        result = self.question.check_if_answered(1)
        self.assertIsInstance(result, UserAnswer)

    def test_check_if_answered_2(self):
        result = self.question.check_if_answered(1055501)
        self.assertFalse(result)

    def test_get_answer(self):
        result = Question.get_answer(1, session)
        self.assertIsInstance(result, Answer)

    def test_finish_poll(self):
        result = Question.finish_poll(1001, session)
        self.assertIsInstance(result, Question)

    def test_finish_poll_2(self):
        result = Question.finish_poll(1055501, session)
        self.assertFalse(result)

    # User Methods
    def test_init_user(self):
        result = User.init_user(1001, 'test_frist_name', 'test_last_name', 'test_username', session)
        self.assertIsInstance(result, User)

    def test_check_user_exist(self):
        result = User.check_user_exist(1001, session)
        self.assertIsInstance(result, User)

    def test_get_groups_where_administrator(self):
        result = self.user.get_groups_where_administrator()
        self.assertIsInstance(result, list)

    def test_get_user_groups(self):
        result = self.user.get_user_groups()
        self.assertIsInstance(result, list)

    def test_vote(self):
        result = User.vote(1001, 1, session)
        self.assertTrue(result)

    # Group Methods
    def test_init_group(self):
        result = Group.init_group(1002, 'public', 'test_group', session)
        self.assertIsInstance(result, Group)

    def test_group_exist(self):
        result = Group.group_exist(1002, session)
        self.assertIsInstance(result, Group)

    def test_get_by_id(self):
        result = Group.get_by_id(1, session)
        self.assertIsInstance(result, Group)

    def test_refresh_admins(self):
        result = self.group.refresh_admins([], session)
        if result:
            self.assertTrue(result)

    def test_get_user_warnings(self):
        result = self.group.get_user_warnings(1001)
        self.assertIsInstance(result, list)

    def test_set_link(self):
        result = self.group.set_link('test_link', session)
        self.assertTrue(result)

    def test_set_rules(self):
        result = self.group.set_rules('test_rules', session)
        self.assertTrue(result)

    def test_set_welcome_message(self):
        result = self.group.set_welcome_message('test_welcome_message', session)
        self.assertTrue(result)

    # GroupUsers
    def test_connect(self):
        result = GroupUser.connect(1, 1, 1, session)
        self.assertTrue(result)

    # UserSession methods
    def test_select_group(self):
        result = UserSession.select_group(self.user, 1, session)
        self.assertIsInstance(result, UserSession)

    def test_stop_ready_to_write(self):
        result = self.user_session.stop_ready_to_write(session)
        self.assertTrue(result)

    def test_get_ready_to_write(self):
        result = self.user_session.get_ready_to_write(session)
        self.assertTrue(result)

    # GroupSettings method
    def test_init_settings(self):
        result = GroupSettings.init_settings(self.group, session)
        self.assertIsInstance(result, GroupSettings)

    def test_set_mute_time(self):
        result = self.group_settings.set_mute_time(10)
        self.assertTrue(result)

    def test_set_warnings_punishment(self):
        result = self.group_settings.set_warnings_punishment(2)
        self.assertTrue(result)

    def test_set_warnings_spoiler_time(self):
        result = self.group_settings.set_warnings_spoiler_time(10)
        self.assertTrue(result)

    def test_set_warnings(self):
        result = self.group_settings.set_warnings(10)
        self.assertTrue(result)

    # Warnings Methods
    def test_warn_user(self):
        result = Warnings.warn_user(1001, 1002, session)
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()
