include /etc/firejail/disable-common.inc
include /etc/firejail/disable-passwdmgr.inc
include /etc/firejail/disable-programs.inc

apparmor
caps.drop all
caps.keep net_raw
netfilter
dns 127.0.0.53
nonewprivs
noroot
seccomp
shell none
private-etc none
private-srv none
private-dev
private-tmp
private-opt none
whitelist ${HOME}/freecircle-bot
whitelist ${HOME}/fcbot-var