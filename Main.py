import sys
import logging
import requests
from Classes import *
from telegram.ext import Updater, ChatMemberHandler
from telegram.ext import CommandHandler, InlineQueryHandler
from telegram.ext import MessageHandler, CallbackQueryHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ChatPermissions, \
    KeyboardButton, ReplyKeyboardMarkup, Update, ChatMember
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
path = config['DATABASE']['PATH']

"""
 vedo update e context che vengono passati a quasi ogni funzione, separare poi funzioni per 
gli i add_handler in un altro file e fare una classe per il resto 
"""

try:
    TOKEN = sys.argv[1]
except IndexError:
    message = 'Please Write the telegram token as argument when you start the bot, for example: ' \
              '"python3 Main.py xadxasa42XAG5XARFXAF%SXRFADFF_G32"'
    # raise IndexError(message)
    TOKEN = '488001898:AAGKjYJ4XxpedIa9Z-762vSc31QmxxfM_r0'  # to run tests, comment the line above, and uncomment this one

updater = Updater(token=TOKEN, use_context=True)
bot = updater.bot
dispatcher = updater.dispatcher
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)
# sqlalchemy session
engine = create_engine(path, echo=False)
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()


def start(update, context):
    context.bot.send_message(chat_id=update.effective_chat.id, text="Benvenuto sul Bot che serve ad amministrare i "
                                                                    "tuoi fantastici gruppi.")


def show_menu(update, context):
    user_id = update.message.from_user.id
    chat_id = update.message.chat.id
    if update.message.chat.type == 'private':
        user_session = check_session(update, context, user_id)
        if user_session:
            group = user_session.group
            create_menu(chat_id, group, context=context)
    else:
        if is_admin(user_id, chat_id):
            context.bot.send_message(chat_id=user_id, text='Ciao, usa pure quì i comandi per modificare le '
                                                           'impostazioni, de hai bisogno di aiuto digita il '
                                                           'comando "/settings".')
        else:
            pass


def create_menu(chat_id, group, message=None, context=None):
    keyboard = [
        [InlineKeyboardButton('Messaggio di benvenuto', callback_data='welcomemessage:start')],
        [InlineKeyboardButton('Regole', callback_data='rules:start'),
         InlineKeyboardButton('Link', callback_data='link:start')],
        [InlineKeyboardButton('Gestione Avvisi', callback_data='warnings:home')],
        [InlineKeyboardButton('Crea Sondaggio', callback_data='createpoll:home')],
        [InlineKeyboardButton('Invia Ultimo Sondaggio', callback_data='createpoll:send')],
        [InlineKeyboardButton('Seleziona Gruppo', callback_data='selectgroup:home')],
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    sender = context.bot if context else bot
    if message:
        sender.edit_message_text(chat_id=chat_id,
                                 text=f'Benvenuto, questo è il pannello di controllo per il gruppo '
                                      f'<strong>{group.title}</strong> seleziona le impostazioni che '
                                      f'vuoi modificare:', reply_markup=reply_markup, parse_mode='HTML',
                                 message_id=message.message_id)
    else:
        sender.send_message(chat_id=chat_id,
                            text=f'Benvenuto, questo è il pannello di controllo per il gruppo '
                                 f'<strong>{group.title}</strong> seleziona le impostazioni che '
                                 f'vuoi modificare:', reply_markup=reply_markup, parse_mode='HTML')


def show_rules(update, context):
    group = Group.group_exist(update.effective_chat.id, session)
    rules = group.rules
    if rules:
        context.bot.send_message(chat_id=update.effective_chat.id, text=rules, parse_mode='HTML')


def show_link(update, context):
    group = Group.group_exist(update.effective_chat.id, session)
    link = group.link
    if link:
        context.bot.send_message(chat_id=update.effective_chat.id, text=link, parse_mode='HTML')

def welcome(update, context):
    chat_name = update.chat_member.chat.title
    ID = update.chat_member.new_chat_member.user.id
    user_status = update.chat_member.new_chat_member.status
    FIRST_NAME = update.chat_member.new_chat_member.user.first_name
    LAST_NAME = update.chat_member.new_chat_member.user.last_name
    USERNAME = update.chat_member.new_chat_member.user.username
    TITLE = update.chat_member.chat.title

    group = Group.group_exist(update.chat_member.chat.id, session)
    welcome_text = group.welcome_message.replace('{FIRST_NAME}', FIRST_NAME)
    welcome_text = welcome_text.replace('{LAST_NAME}', LAST_NAME if LAST_NAME else ' ')
    welcome_text = welcome_text.replace('{USERNAME}', USERNAME if USERNAME else ' ')
    welcome_text = welcome_text.replace('{TITLE}', TITLE if TITLE else ' ')
    welcome_text = welcome_text.replace('{ID}', str(ID))
    if user_status == ChatMember.MEMBER:
        context.bot.send_message(chat_id=update.chat_member.chat.id, text=welcome_text, parse_mode='HTML')
    rules = group.rules
    if rules:
        try:
            if user_status == ChatMember.MEMBER:
                context.bot.send_message(chat_id=ID, text=f'Le regole del gruppo {group.title} sono:\n' + rules,
                                     parse_mode='HTML')
        except Exception as exc:
            print('eccezione : ', exc)
            if user_status == ChatMember.MEMBER:
                context.bot.send_message(chat_id=update.chat_member.chat.id, text=rules, parse_mode='HTML')
    init_user_group_member_update(update, context)
    if check_if_blacklisted(ID):
        if user_status == ChatMember.MEMBER:
            context.bot.send_message(chat_id=update.chat_member.chat.id, text='ATTENZIONE:\nL\'utente è stato precedentemente'
                                                                        ' per spam, è consigliabile il ban')


def check_if_blacklisted(user_id):
    r = requests.get(f'https://api.cas.chat/check?user_id={user_id}')
    return r.json()["ok"]


def send_poll(update, context):
    user_id = update.message.from_user.id
    question = Question.get_last_question(user_id, session)
    if question:
        if update.message.chat.type == 'private':
            user = User.check_user_exist(user_id, session)
            groups = user.get_groups_where_administrator()
            g_keyboard = []
            for group in groups:
                g_keyboard.append([InlineKeyboardButton(group.title,
                                                        callback_data='grouptosendpoll:' + str(group.telegram_id))])
            reply_markup = InlineKeyboardMarkup(g_keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text="Seleziona il gruppo o i gruppi dove vuoi inviare il sondaggio:\n",
                                     reply_markup=reply_markup)
        else:
            keyboard = []
            for answer in question.answers:
                keyboard.append([InlineKeyboardButton(answer.content, callback_data='vote:' + str(answer.id))])
            reply_markup = InlineKeyboardMarkup(keyboard)
            message = context.bot.send_message(chat_id=update.effective_chat.id, text=question.content,
                                               reply_markup=reply_markup)
            update_poll_votes(question, message)


def button(update, context):
    query = update.callback_query
    message = query.message
    user_id = update.effective_user.id
    if 'vote:' in query.data:
        answer_id = query.data.split(':')
        answer = Question.get_answer(answer_id[1], session)
        question = answer.question
        user = User.check_user_exist(user_id, session)
        vote = question.check_if_answered(user.id)
        if vote:
            if vote.answer_id == int(answer_id[1]):
                session.delete(vote)
                session.commit()
            else:
                session.delete(vote)
                session.commit()
                User.vote(user.id, answer_id[1], session)
        else:
            User.vote(user.id, answer_id[1], session)
        update_poll_votes(question, message, context)
    elif 'session-group:' in query.data:
        group_id = query.data.split(':')
        group_id = group_id[1]
        select_session(update, context, user_id, group_id, message)
    elif 'selectgroup:' in query.data:
        if 'home' in query.data:
            user = User.check_user_exist(user_id, session)
            groups_list = user.get_groups_where_administrator()
            keyboard = []
            if len(groups_list) > 0:
                for group in groups_list:
                    keyboard.append(
                        [InlineKeyboardButton(group.title, callback_data='session-group:' + str(group.id))])
                reply_markup = InlineKeyboardMarkup(keyboard)
                context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                              text='<strong>Attenzione</strong>.\nSeleziona un '
                                                   'gruppo per poter procedere.',
                                              reply_markup=reply_markup,
                                              parse_mode='HTML',
                                              message_id=message.message_id)
    elif 'menu:' in query.data:
        user = User.check_user_exist(user_id, session)
        create_menu(user_id, user.session[0].group, message, context)
    elif 'warnings:' in query.data:
        if 'home' in query.data:
            user = User.check_user_exist(user_id, session)
            if not user.session[0].group.setting:
                GroupSettings.init_settings(user.session[0].group, session)
            settings = user.session[0].group.setting[0]
            punishments = ['nulla', 'muto', 'kick', 'ban']
            txt = f"Da qui, puoi settare le impostazioni degli avvisi per adattarle al " \
                  f"gruppo <strong>{user.session[0].group.title}</strong>.\nAttualmente il " \
                  f"numero massimo di warnings è <strong>{settings.warnings}</strong>, ogni warning scade" \
                  f" dopo <strong>{settings.warning_spoiler_time}</strong> ore, e " \
                  f"la punizione per il comportamento è " \
                  f"<strong>{punishments[settings.warning_punishment]}</strong>.\n\nTi ricordo che " \
                  f"puoi mandare avvisi di " \
                  f"scorretto comportamento direttamente nel gruppo che vuoi amministrare rispondendo al " \
                  f"messaggio che " \
                  f"vuoi segnalare con il comando '/warn' (il messaggio citato e il comando lanciato verranno " \
                  f"automaticamente eliminati, e il bot invierà un avviso automatico nel gruppo per segnalare all' " \
                  f"utente il proprio comportamento ed invitarlo a correggere eventuali " \
                  f"comportamenti inadeguati al tuo gruppo.)"
            keyboard = [
                [InlineKeyboardButton('Avvisi Massimi', callback_data='warnings:num_trigger')],
                [InlineKeyboardButton('Scadenza Avvisi', callback_data='warnings:spoiler_time')],
                [InlineKeyboardButton('punizione Avvisi', callback_data='warnings:punishment')],
                [InlineKeyboardButton('Tempo di muto', callback_data='mute:time')],
                [InlineKeyboardButton('Indietro', callback_data='menu:start')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.edit_message_text(text=txt, chat_id=message.chat_id, reply_markup=reply_markup,
                                          parse_mode='HTML',
                                          message_id=message.message_id)
        elif 'num_trigger' in query.data:
            user = User.check_user_exist(user_id, session)
            settings = user.session[0].group.setting[0]
            if query.data != 'warnings:num_trigger':
                number = query.data.split(':')
                number = number[2]
                settings.set_warnings(number)
                session.add(settings)
                session.commit()
            keyboard = [
                [InlineKeyboardButton('3', callback_data='warnings:num_trigger:3'),
                 InlineKeyboardButton('5', callback_data='warnings:num_trigger:5')],
                [InlineKeyboardButton('10', callback_data='warnings:num_trigger:10'),
                 InlineKeyboardButton('15', callback_data='warnings:num_trigger:15')],
                [InlineKeyboardButton('30', callback_data='warnings:num_trigger:30'),
                 InlineKeyboardButton('50', callback_data='warnings:num_trigger:50')],
                [InlineKeyboardButton('Indietro', callback_data='warnings:home')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.edit_message_text(
                text=f'Attualmente, per il gruppo <strong>{user.session[0].group.title}</strong> il '
                     f'numero di warnings necessari per far scattare la punizione '
                     f'sono <strong>{settings.warnings}</strong>: clicca uno dei seguenti '
                     f'pulsanti per modificare il '
                     f'parametro', chat_id=message.chat_id, reply_markup=reply_markup, parse_mode='HTML',
                message_id=message.message_id)
        elif 'spoiler_time' in query.data:
            user = User.check_user_exist(user_id, session)
            settings = user.session[0].group.setting[0]
            if query.data != 'warnings:spoiler_time':
                number = query.data.split(':')
                number = number[2]
                settings.set_warnings_spoiler_time(number)
                session.add(settings)
                session.commit()
            keyboard = [
                [InlineKeyboardButton('1h', callback_data='warnings:spoiler_time:1'),
                 InlineKeyboardButton('12h', callback_data='warnings:spoiler_time:12')],
                [InlineKeyboardButton('1 giorno', callback_data='warnings:spoiler_time:24'),
                 InlineKeyboardButton('2 giorni', callback_data='warnings:spoiler_time:48')],
                [InlineKeyboardButton('3 giorni', callback_data='warnings:spoiler_time:72'),
                 InlineKeyboardButton('1 settimana', callback_data='warnings:spoiler_time:168')],
                [InlineKeyboardButton('Indietro', callback_data='warnings:home')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.edit_message_text(text=f'Attualmente il tempo di validità di un warning peri il gruppo '
                                               f'<strong>{user.session[0].group.title}</strong> è '
                                               f'<strong>{settings.warning_spoiler_time}</strong>H.\n '
                                               f'clicca uno dei seguenti pulsanti per modificare il '
                                               f'parametro', chat_id=message.chat_id, reply_markup=reply_markup,
                                          parse_mode='HTML',
                                          message_id=message.message_id)
        elif 'punishment' in query.data:
            user = User.check_user_exist(user_id, session)
            settings = user.session[0].group.setting[0]
            if query.data != 'warnings:punishment':
                number = query.data.split(':')
                number = number[2]
                settings.set_warnings_punishment(number)
                session.add(settings)
                session.commit()
            punishments = ['nulla', 'muto', 'kick', 'ban']
            keyboard = [
                [InlineKeyboardButton('nessuna', callback_data='warnings:punishment:0')],
                [InlineKeyboardButton('mute', callback_data='warnings:punishment:1')],
                [InlineKeyboardButton('kick', callback_data='warnings:punishment:2')],
                [InlineKeyboardButton('ban', callback_data='warnings:punishment:3')],
                [InlineKeyboardButton('Indietro', callback_data='warnings:home')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.edit_message_text(text=f'Attualmente la punizione per il warning nel gruppo '
                                               f'<strong>{user.session[0].group.title}</strong> è '
                                               f'<strong>{punishments[settings.warning_punishment]}</strong>: clicca uno '
                                               f'dei seguenti pulsanti '
                                               f'per modificare il parametro',
                                          chat_id=message.chat_id, reply_markup=reply_markup, parse_mode='HTML',
                                          message_id=message.message_id)
    elif 'mute' in query.data:
        user = User.check_user_exist(user_id, session)
        settings = user.session[0].group.setting[0]
        if query.data != 'mute:time':
            number = query.data.split(':')
            number = number[2]
            settings.set_mute_time(number)
            session.add(settings)
            session.commit()
        keyboard = [
            [InlineKeyboardButton('5 minuti', callback_data='mute:time:5')],
            [InlineKeyboardButton('10 minuti', callback_data='mute:time:10')],
            [InlineKeyboardButton('1 ora', callback_data='mute:time:60')],
            [InlineKeyboardButton('12 ore', callback_data='mute:time:720')],
            [InlineKeyboardButton('1 giorno', callback_data='mute:time:1440')],
            [InlineKeyboardButton('Indietro', callback_data='warnings:home')],
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        context.bot.edit_message_text(text=f'Attualmente il tempo di muto per il gruppo '
                                           f'<strong>{user.session[0].group.title}</strong> è di'
                                           f' <strong>{settings.mute_time}</strong> Minuti: clicca uno dei '
                                           f'seguenti pulsanti '
                                           f'per modificare il parametro',
                                      chat_id=message.chat_id, reply_markup=reply_markup, parse_mode='HTML',
                                      message_id=message.message_id)
    elif 'welcomemessage' in query.data:
        if 'save' in query.data:
            user = User.check_user_exist(user_id, session)
            wm = user.session[0].other.split('wm#1:')
            user.session[0].group.set_welcome_message(wm[1], session)
            context.bot.edit_message_text(text='<strong>Modifica Completata</strong>', chat_id=message.chat_id,
                                          message_id=message.message_id, parse_mode='HTML')
            user.session[0].stop_ready_to_write(session)
            create_menu(user_id, user.session[0].group, context=context)
        elif 'start' in query.data:
            user_session = check_session(update, context, user_id)
            keyboard = [
                [InlineKeyboardButton('Indietro', callback_data='rules:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            if user_session:
                group = user_session.group
                if group.welcome_message and len(group.welcome_message) > 3:
                    context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                                  text=f'Attualmente Il messaggio di benvenuto settato per il gruppo '
                                                       f'<strong>{group.title}</strong> è :', parse_mode='HTML',
                                                  message_id=message.message_id)
                    context.bot.send_message(chat_id=update.effective_chat.id, text=group.welcome_message)
                else:
                    context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                                  text=f'Attualmente Nessun messaggio di benvenuto è'
                                                       f' stato settato per il gruppo'
                                                       f' <strong>{group.title}</strong>.', parse_mode='HTML',
                                                  message_id=message.message_id)
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text='Invia di seguito il nuovo messaggio di benvenuto, o clicca sul '
                                              'pulsante Indietro per tornare al menu principale'
                                              'puoi usare {ID} {FIRST_NAME}, {LAST_NAME}, {USERNAME} per '
                                              'indicare gruppo e {TITLE} per indicare il nome del gruppo, per esempio '
                                              '"Benvenuto {USERNAME} nel gruppo {TITLE}"', parse_mode='HTML',
                                         reply_markup=reply_markup)
                user_session.get_ready_to_write('xwm#1:')
        elif 'undo' in query.data:
            user = User.check_user_exist(user_id, session)
            context.bot.edit_message_text(text='<strong>Modifica Annullata</strong>', chat_id=message.chat_id,
                                          message_id=message.message_id, parse_mode='HTML')
            user.session[0].stop_ready_to_write(session)
            create_menu(user_id, user.session[0].group, context=context)
    elif 'rules' in query.data:
        if 'save' in query.data:
            user = User.check_user_exist(user_id, session)
            wm = user.session[0].other.split('r#1:')
            user.session[0].group.set_rules(wm[1], session)
            context.bot.edit_message_text(text='<strong>Modifica Completata</strong>', chat_id=message.chat_id,
                                          message_id=message.message_id, parse_mode='HTML')
            user.session[0].stop_ready_to_write(session)
            create_menu(user_id, user.session[0].group, context=context)
        elif 'start' in query.data:
            user_session = check_session(update, context, user_id)
            group = user_session.group
            keyboard = [
                [InlineKeyboardButton('Indietro', callback_data='rules:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            if group.rules and len(group.rules) > 3:
                context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                              text=f'Attualmente le regole settate per il gruppo '
                                                   f'<strong>{group.title}</strong> sono :', parse_mode='HTML',
                                              message_id=message.message_id)
                context.bot.send_message(chat_id=update.effective_chat.id, text=group.rules)
            else:
                context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                              text=f'Attualmente regole non settate per il gruppo'
                                                   f' <strong>{group.title}</strong>.', parse_mode='HTML',
                                              message_id=message.message_id)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f'invia di seguito le nuove regole da impostare per il gruppo '
                                          f'<strong>{group.title}</strong>, oppure clicca su indietro per '
                                          f'tornare al menu principale :', parse_mode='HTML',
                                     reply_markup=reply_markup)
            user_session.get_ready_to_write('xr#1:')
        elif 'undo' in query.data:
            user = User.check_user_exist(user_id, session)
            context.bot.edit_message_text(text='<strong>Modifica Annullata</strong>', chat_id=message.chat_id,
                                          message_id=message.message_id, parse_mode='HTML')
            user.session[0].stop_ready_to_write(session)
            create_menu(user_id, user.session[0].group, context=context)
    elif 'link' in query.data:
        if 'save' in query.data:
            user = User.check_user_exist(user_id, session)
            wm = user.session[0].other.split('l#1:')
            user.session[0].group.set_link(wm[1], session)
            context.bot.edit_message_text(text='<strong>Modifica Completata</strong>', chat_id=message.chat_id,
                                          message_id=message.message_id, parse_mode='HTML')
            user.session[0].stop_ready_to_write(session)
            create_menu(user_id, user.session[0].group, context=context)
        elif 'start' in query.data:
            user_session = check_session(update, context, user_id)
            group = user_session.group
            keyboard = [
                [InlineKeyboardButton('Indietro', callback_data='link:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            if group.link and len(group.link) > 3:
                context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                              text=f'Attualmente il link d\' invito settato per il gruppo '
                                                   f'<strong>{group.title}</strong> è :', parse_mode='HTML',
                                              message_id=message.message_id)
                context.bot.send_message(chat_id=update.effective_chat.id, text=group.link)
            else:
                context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                              text=f'Attualmente link d\'invito publico non settato per il gruppo'
                                                   f' <strong>{group.title}</strong>.', parse_mode='HTML',
                                              message_id=message.message_id)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f'invia di seguito il nuovo link da impostare per il gruppo '
                                          f'<strong>{group.title}</strong>, oppure clicca su indietro per '
                                          f'tornare al menu principale :', parse_mode='HTML',
                                     reply_markup=reply_markup)
            user_session.get_ready_to_write('xl#1:')
        elif 'undo' in query.data:
            user = User.check_user_exist(user_id, session)
            context.bot.edit_message_text(text='<strong>Modifica Annullata</strong>', chat_id=message.chat_id,
                                          message_id=message.message_id, parse_mode='HTML')
            user.session[0].stop_ready_to_write(session)
            create_menu(user_id, user.session[0].group, context=context)
    elif 'createpoll' in query.data:
        if 'continue1' in query.data:
            user = User.check_user_exist(user_id, session)
            wm = user.session[0].other.split('q#2:')
            try:
                Question.init_question(wm[1], user_id, session)
                context.bot.edit_message_text(text='✔️', chat_id=message.chat_id, message_id=message.message_id)
                context.bot.send_message(message.chat_id, 'Invia adesso le risposte alla domanda che hai creato prima.')
                user.session[0].get_ready_to_write('xq#3:')
            except IndexError:
                create_menu(user_id, user.session[0].group, context=context)
        elif 'home' in query.data:
            user = User.check_user_exist(user_id, session)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text="Scrivi di seguito la domanda che vuoi porre:")
            user.session[0].get_ready_to_write('xq#1:')
        elif 'continue2' in query.data:
            user = User.check_user_exist(user_id, session)
            wm = user.session[0].other.split('q#4:')
            try:
                if not Question.check_answer(user_id, wm[1], session):
                    Question.add_answer(user_id, wm[1], session)
                    bot.edit_message_text('✔️', chat_id=message.chat_id, message_id=message.message_id)
                    bot.send_message(message.chat_id, 'Invia adesso le risposte alla domanda che hai creato prima.')
                    user.session[0].get_ready_to_write('xq#5:')
                else:
                    bot.edit_message_text('✔️', chat_id=message.chat_id, message_id=message.message_id)
                    bot.send_message(message.chat_id, 'Impossibile aggiungere 2 risposte identiche, per '
                                                      'piacere scrivi una risposta valida.')
                    user.session[0].get_ready_to_write('xq#5:')
            except IndexError:
                pass
        elif 'finish' in query.data:
            user = User.check_user_exist(user_id, session)
            wm = user.session[0].other.split('q#4:')
            try:
                Question.add_answer(user_id, wm[1], session)
                Question.complete_poll(user_id, session)
                bot.edit_message_text('✔️', chat_id=message.chat_id, message_id=message.message_id)
                bot.send_message(message.chat_id, 'Sondaggio completato, a quale gruppo vuoi inoltrare il sondaggio?')
                user.session[0].stop_ready_to_write(session)
                groups = user.get_groups_where_administrator()
                g_keyboard = []
                for group in groups:
                    g_keyboard.append([InlineKeyboardButton(group.title,
                                                            callback_data='grouptosendpoll:' + str(group.telegram_id))])
                g_keyboard.append([InlineKeyboardButton('Torna al Menu', callback_data='menu:')])
                reply_markup = InlineKeyboardMarkup(g_keyboard)
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text="Seleziona il gruppo o i gruppi dove vuoi inviare il sondaggio:\n",
                                         reply_markup=reply_markup)
            except IndexError:
                create_menu(user_id, user.session[0].group, context=context)
        elif 'send' in query.data:
            user = User.check_user_exist(user_id, session)
            context.bot.edit_message_text(text='✔️', chat_id=message.chat_id, message_id=message.message_id)
            context.bot.send_message(message.chat_id, 'A quale gruppo vuoi inoltrare il sondaggio?')
            user.session[0].stop_ready_to_write(session)
            groups = user.get_groups_where_administrator()
            g_keyboard = []
            for group in groups:
                g_keyboard.append([InlineKeyboardButton(group.title,
                                                        callback_data='grouptosendpoll:' + str(group.telegram_id))])
            g_keyboard.append([InlineKeyboardButton('Torna al Menu', callback_data='menu:')])
            reply_markup = InlineKeyboardMarkup(g_keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text="Seleziona il gruppo o i gruppi dove vuoi inviare il sondaggio:\n",
                                     reply_markup=reply_markup)
        elif 'undo' in query.data:
            user = User.check_user_exist(user_id, session)
            context.bot.edit_message_text(text='Creazione sondaggio annullata.', chat_id=message.chat_id,
                                          message_id=message.message_id, parse_mode='HTML')
            user.session[0].stop_ready_to_write(session)
            create_menu(user_id, user.session[0].group, message, context)
        elif 'modify1' in query.data:
            user = User.check_user_exist(user_id, session)
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text="Scrivi di seguito la domanda che vuoi porre:")
            user.session[0].get_ready_to_write('xq#1:')
    elif 'grouptosendpoll' in query.data:
        user = User.check_user_exist(user_id, session)
        group = query.data.split(':')
        question = Question.get_last_question(user_id, session)
        keyboard = []
        for answer in question.answers:
            keyboard.append([InlineKeyboardButton(answer.content, callback_data='vote:' + str(answer.id))])
        reply_markup = InlineKeyboardMarkup(keyboard)
        context.bot.send_message(chat_id=group[1], text=f'{user.first_name} desidera condividere con '
                                                        f'voi il seguente sondaggio:\n')
        message = context.bot.send_message(chat_id=group[1], text=question.content,
                                           reply_markup=reply_markup)
        update_poll_votes(question, message, context)
    else:
        print('Query data : ', query.data)


def update_poll_votes(question, message, context=None):
    sender = context.bot if context else bot
    txt = question.content + "\n"
    for answer in question.answers:
        txt += '  -> <strong>' + answer.content + ':</strong>\n'
        for vote in answer.users:
            txt += '    - <em>' + vote.user.first_name + ';</em>\n'
    keyboard = []
    for answer in question.answers:
        keyboard.append([InlineKeyboardButton(answer.content, callback_data='vote:' + str(answer.id))])
    reply_markup = InlineKeyboardMarkup(keyboard)
    sender.edit_message_text(text=txt, chat_id=message.chat_id, reply_markup=reply_markup, parse_mode='HTML',
                             message_id=message.message_id)


def set_welcome_message(update, context):
    user_id = update.message.from_user.id
    chat_id = update.message.chat.id
    if update.message.chat.type == 'private':
        user_session = check_session(update, context, user_id)
        if user_session:
            group = user_session.group
            if group.welcome_message and len(group.welcome_message) > 3:
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text=f'Attualmente Il messaggio di benvenuto settato per il gruppo '
                                              f'<strong>{group.title}</strong> è :', parse_mode='HTML')
                context.bot.send_message(chat_id=update.effective_chat.id, text=group.welcome_message)
            else:
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text=f'Attualmente Nessun messaggio di benvenuto è stato settato per il gruppo'
                                              f' <strong>{group.title}</strong>.', parse_mode='HTML')
            keyboard = [
                [InlineKeyboardButton('Salva', callback_data='welcomemessage:save')],
                [InlineKeyboardButton('Annulla', callback_data='welcomemessage:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='Invia il nuovo messaggio di benvenuto '
                                                                            'e premi salva per modificare '
                                                                            'l\' attuale messaggio di benvenuto.',
                                     reply_markup=reply_markup)
            user_session.get_ready_to_write()
    else:
        if is_admin(user_id, chat_id):
            context.bot.send_message(chat_id=user_id, text='Ciao, usa pure quì i comandi per modificare le '
                                                           'impostazioni, de hai bisogno di aiuto digita il '
                                                           'comando "/help".')
        else:
            pass


def set_rules(update, context):
    user_id = update.message.from_user.id
    chat_id = update.message.chat.id
    if update.message.chat.type == 'private':
        user_session = check_session(update, context, user_id)
        if user_session:
            group = user_session.group
            if group.rules and len(group.rules) > 3:
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text=f'Attualmente le regole settate per il gruppo '
                                              f'<strong>{group.title}</strong> sono :', parse_mode='HTML')
                context.bot.send_message(chat_id=update.effective_chat.id, text=group.rules)
            else:
                context.bot.send_message(chat_id=update.effective_chat.id,
                                         text=f'Attualmente regole non settate per il gruppo'
                                              f' <strong>{group.title}</strong>.', parse_mode='HTML')
            user_session.get_ready_to_write()
    else:
        if is_admin(user_id, chat_id):
            context.bot.send_message(chat_id=user_id, text='Ciao, usa pure quì i comandi per modificare le '
                                                           'impostazioni, de hai bisogno di aiuto digita il '
                                                           'comando "/help".')
        else:
            pass


def set_warnings(update, context):
    user_id = update.message.from_user.id
    chat_id = update.message.chat.id
    if update.message.chat.type == 'private':
        user_session = check_session(update, context, user_id)
        if user_session:
            group = user_session.group
            if not group.setting:
                GroupSettings.init_settings(group, session)
            settings = group.setting[0]
            punishments = ['nulla', 'muto', 'kick', 'ban']
            txt = f"Da qui, puoi settare le impostazioni degli avvisi per adattarle al " \
                  f"gruppo <strong>{group.title}</strong>.\nAttualmente il " \
                  f"numero massimo di avvisi è <strong>{settings.warnings}</strong>, ogni avviso scade" \
                  f" dopo <strong>{settings.warning_spoiler_time}</strong> ore, e " \
                  f"la punizione per il comportamento è " \
                  f"<strong>{punishments[settings.warning_punishment]}</strong> "
            keyboard = [
                [InlineKeyboardButton('Max Warnings', callback_data='warnings:num_trigger')],
                [InlineKeyboardButton('Warnings Time', callback_data='warnings:spoiler_time')],
                [InlineKeyboardButton('Warnings Punishment', callback_data='warnings:punishment')],
                [InlineKeyboardButton('Mute Time', callback_data='mute:time')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text=txt,
                                     reply_markup=reply_markup, parse_mode='HTML')
    else:
        if is_admin(user_id, chat_id):
            context.bot.send_message(chat_id=user_id, text='Ciao, usa pure quì i comandi per modificare le '
                                                           'impostazioni, se hai bisogno di aiuto digita il '
                                                           'comando "/help".')
        else:
            pass


def check_session(update, context, user_id):
    user = User.check_user_exist(user_id, session)
    if not user.session:
        groups_list = user.get_groups_where_administrator()
        keyboard = []
        if len(groups_list) > 0:
            for group in groups_list:
                keyboard.append([InlineKeyboardButton(group.title, callback_data='session-group:' + str(group.id))])
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='<strong>Attenzione</strong>, nessun '
                                                                            'gruppo selezionato, seleziona un gruppo '
                                                                            'per poter procedere.',
                                     reply_markup=reply_markup, parse_mode='HTML')
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text='<strong>Attenzione</strong>, Non sei admin'
                                                                            ' di alcun gruppo, controlla se questo bot '
                                                                            'è presente come amministratore nel gruppo '
                                                                            'in cui desideri fare modifiche, e'
                                                                            ' riprova.', parse_mode='HTML')
    else:
        return user.session[0]


def select_session(update, context, user_id, group_id, message):
    user = User.check_user_exist(user_id, session)
    if not user.session:
        user_session = UserSession.select_group(user, group_id, session)
        context.bot.edit_message_text(chat_id=update.effective_chat.id, text=f'Gruppo {user_session.group.title} '
                                                                             f'selezionato.', parse_mode='HTML',
                                      message_id=message.message_id)
        create_menu(user_id, user_session.group, context=context)
    else:
        user.session[0].group_id = group_id
        session.add(user)
        session.commit()
        context.bot.edit_message_text(chat_id=update.effective_chat.id,
                                      text=f'Gruppo {user.session[0].group.title} '
                                           f'selezionato.', parse_mode='HTML', message_id=message.message_id)
        create_menu(user_id, user.session[0].group, context=context)


def is_admin(user_id, chat_id, context=None):
    sender = context.bot if context else bot
    admin_list = sender.get_chat_administrators(chat_id)
    for admin in admin_list:
        if user_id == admin.user.id:
            return True
    else:
        return False


def refresh_admins(chat_id, context=None):
    sender = context.bot if context else bot
    admin_list = sender.get_chat_administrators(chat_id)
    group = Group.group_exist(chat_id, session)
    group.refresh_admins(admin_list, session)


def init_user_group(update, context=None):
    user_id = update.message.from_user.id
    user_first_name = update.message.from_user.first_name
    user_last_name = update.message.from_user.last_name
    user_username = update.message.from_user.username
    group = Group.init_group(update.effective_chat.id, update.effective_chat.type, update.effective_chat.title, session)
    user = User.init_user(user_id, user_first_name, user_last_name, user_username, session)
    GroupUser.connect(user.id, group.id, is_admin(user_id, update.effective_chat.id, context), session)
    
def init_user_group_member_update(update, context=None):
    user_id = update.chat_member.new_chat_member.user.id
    user_status = update.chat_member.new_chat_member.status
    user_first_name = update.chat_member.new_chat_member.user.first_name
    user_last_name = update.chat_member.new_chat_member.user.last_name
    user_username = update.chat_member.new_chat_member.user.username
    group = Group.init_group(update.chat_member.chat.id, update.chat_member.chat.type, update.chat_member.chat.title, session)
    user = User.init_user(user_id, user_first_name, user_last_name, user_username, session)
    GroupUser.connect(user.id, group.id, is_admin(user_id, update.chat_member.chat.id, context), session)


def warn(update, context):
    user_id = update.message.from_user.id
    chat_id = update.effective_chat.id
    if is_admin(user_id, chat_id, context):
        target_message = update.message.reply_to_message
        target_id = update.message.reply_to_message.from_user.id
        target_name = update.message.reply_to_message.from_user.first_name
        target_user = User.check_user_exist(target_id, session)
        group = Group.group_exist(chat_id, session)
        warnings_limit = group.setting[0].warnings
        Warnings.warn_user(target_user.id, group.id, session)
        total_warnings = len(group.get_user_warnings(target_user.id))
        if not is_admin(target_user.telegram_id, chat_id, context):
            context.bot.send_message(chat_id=chat_id, text=f'<strong>Attenzione</strong> {target_name}, hai ricevuto '
                                                           f'un avviso per condotta scorretta, '
                                                           f'attualmente sei a {total_warnings} '
                                                           f'su {warnings_limit}.', parse_mode='HTML')
            context.bot.delete_message(chat_id=chat_id, message_id=target_message.message_id)
            context.bot.delete_message(chat_id=chat_id, message_id=update.message.message_id)
            if total_warnings >= warnings_limit:
                punish(target_user, group, 'warnings')
        else:
            context.bot.send_message(chat_id=chat_id, text='Mi dispiace, non posso punire un admin', parse_mode='HTML')
    else:
        context.bot.send_message(chat_id=chat_id, text='Mi dispiace, solo gli admin possono '
                                                       'utilizzare questo comando.', parse_mode='HTML')


def punish(user, group, motivation, context=None):
    sender = context.bot if context else bot
    if motivation == 'warnings':
        punishment = group.setting[0].warning_punishment
        if punishment == 1:
            permissions = ChatPermissions(can_send_messages=False)
            mute_time = group.setting[0].mute_time
            sender.restrict_chat_member(chat_id=group.telegram_id, user_id=user.telegram_id, permissions=permissions,
                                        until_date=datetime.datetime.now() + datetime.timedelta(minutes=mute_time))
            sender.send_message(chat_id=group.telegram_id, text='Utente mutato.', parse_mode='HTML')
        elif punishment == 2:
            sender.kick_chat_member(chat_id=group.telegram_id, user_id=user.telegram_id,
                                    until_date=datetime.datetime.now() + datetime.timedelta(seconds=60))
            sender.send_message(chat_id=group.telegram_id, text='Utente buttato fuori dal gruppo.', parse_mode='HTML')
        elif punishment == 3:
            sender.kick_chat_member(chat_id=group.telegram_id, user_id=user.telegram_id,
                                    until_date=0)
            sender.send_message(chat_id=group.telegram_id, text='Utente Bannato dal gruppo.', parse_mode='HTML')


def echo(update, context):
    user_id = update.message.from_user.id
    if update.message.chat.type == 'private':
        user = User.check_user_exist(user_id, session)
        if 'xwm#1:' in user.session[0].other:
            content = update.message.text
            user.session[0].other = 'xwm#1:' + content
            keyboard = [
                [InlineKeyboardButton('Salva', callback_data='welcomemessage:save')],
                [InlineKeyboardButton('Annulla', callback_data='welcomemessage:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='salva il nuovo messaggio di '
                                                                            'benvenuto per continuare, '
                                                                            'oppure annulla l\' operazione.',
                                     reply_markup=reply_markup)
        elif 'xr#1:' in user.session[0].other:
            content = update.message.text
            user.session[0].other = 'xr#1:' + content
            keyboard = [
                [InlineKeyboardButton('Salva', callback_data='rules:save')],
                [InlineKeyboardButton('Annulla', callback_data='rules:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='Premi salva per modificare '
                                                                            'le regole attuali, oppure annulla per '
                                                                            'tornare indietro',
                                     reply_markup=reply_markup)
        elif 'xl#1:' in user.session[0].other:
            content = update.message.text
            user.session[0].other = 'xl#1:' + content
            keyboard = [
                [InlineKeyboardButton('Salva', callback_data='link:save')],
                [InlineKeyboardButton('Annulla', callback_data='link:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='Premi salva per modificare '
                                                                            'il link del gruppo attuale, oppure '
                                                                            'annulla per '
                                                                            'tornare indietro',
                                     reply_markup=reply_markup)
        elif 'xq#1:' in user.session[0].other:
            user.session[0].other = 'xq#2:' + update.message.text
            keyboard = [
                [InlineKeyboardButton('Continua', callback_data='createpoll:continue1')],
                [InlineKeyboardButton('Modifica', callback_data='createpoll:modify1'),
                 InlineKeyboardButton('Annulla', callback_data='createpoll:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='Perfetto, domanda impostata.\n '
                                                                            'Cosa vuoi fare adesso?',
                                     reply_markup=reply_markup)
        elif 'xq#3:' in user.session[0].other:
            user.session[0].other = 'xq#4:' + update.message.text
            keyboard = [
                [InlineKeyboardButton('Continua', callback_data='createpoll:continue2')],
                [InlineKeyboardButton('Annulla', callback_data='createpoll:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='Perfetto, risposta caricata.\n '
                                                                            'Cosa vuoi fare adesso?',
                                     reply_markup=reply_markup)
        elif 'xq#5:' in user.session[0].other:
            user.session[0].other = 'xq#4:' + update.message.text
            keyboard = [
                [InlineKeyboardButton('Continua', callback_data='createpoll:continue2')],
                [InlineKeyboardButton('Completa', callback_data='createpoll:finish')],
                [InlineKeyboardButton('Annulla', callback_data='createpoll:undo')],
            ]
            reply_markup = InlineKeyboardMarkup(keyboard)
            context.bot.send_message(chat_id=update.effective_chat.id, text='Perfetto, risposta caricata.\n '
                                                                            'Cosa vuoi fare adesso?',
                                     reply_markup=reply_markup)

    else:
        init_user_group(update)
        refresh_admins(update.effective_chat.id)


if __name__ == '__main__':
    dispatcher.add_handler(CommandHandler('start', show_menu))
    dispatcher.add_handler(CommandHandler('settings', show_menu))
    dispatcher.add_handler(CommandHandler('help', show_menu))
    dispatcher.add_handler(CommandHandler('rules', show_rules))
    dispatcher.add_handler(CommandHandler('regole', show_rules))
    dispatcher.add_handler(CommandHandler('link', show_link))
    dispatcher.add_handler(CommandHandler('createpoll', show_menu))
    dispatcher.add_handler(CommandHandler('completepoll', show_menu))
    dispatcher.add_handler(CommandHandler('sendpoll', send_poll))
    dispatcher.add_handler(CommandHandler('welcomemessage', show_menu))
    dispatcher.add_handler(CommandHandler('setrules', show_menu))
    dispatcher.add_handler(CommandHandler('selectgroup', show_menu))
    dispatcher.add_handler(CommandHandler('warnings', show_menu))
    dispatcher.add_handler(CommandHandler('warn', warn))
    dispatcher.add_handler(ChatMemberHandler(welcome, chat_member_types=0))
    dispatcher.add_handler(MessageHandler(Filters.text, echo))
    dispatcher.add_handler(CallbackQueryHandler(button))

    updater.start_polling(allowed_updates=['update_id','message','edited_message','channel_post','edited_channel_post','inline_query','chosen_inline_result','callback_query','shipping_query','pre_checkout_query','poll','poll_answer','my_chat_member','chat_member'])
